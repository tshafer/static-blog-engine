#!/usr/bin/env python
#
# 


import argparse
import staticblog as blog
import os
import sys

from template import BlogTemplate


def updater():
    cli_args = parse_command_line_args()
    
    # Just need the one template --- saves a lot of IO
    post_template = BlogTemplate()
    
    # MODE: update draft files
    if cli_args.drafts == True:
        num_published = 0
        
        if len(cli_args.FILES) == 0:
            blog.log('DRAFT: all-files mode')
            
            for file in map(lambda s: s.strip(), os.listdir(blog.drafts_dir)):
                fullpath = os.path.join(blog.drafts_dir, file)
                # Ignore dotfiles
                if os.path.isfile(fullpath) and file[0:1] != '.':
                    num_published += blog.update_draft_post(fullpath, post_template)
                    blog.log('')
                else:
                    blog.log('!!! Rejected file {:s}'.format(file))
            
        # Check only specified files
        else:
            blog.log('DRAFT: file-by-file mode')
            
            for file in cli_args.FILES:
                num_published += blog.update_draft_post(file, post_template)
        
        blog.log('{:d} files were published'.format(num_published))
        if num_published > 0: blog.rebuild_homepage(post_template)
        
    
    # MODE: update published files
    elif cli_args.published == True:
        blog.log('PUBLISHED')
        
        if len(cli_args.FILES) == 0:
            blog.log('No files were passed to the script')
            return
        else:
            num_updated = 0
            
            # Always do this, even if not different
            for file in cli_args.FILES:
                num_updated += blog.update_published_post(file, post_template)
            
            # Rebuild indexes
            blog.log('{:d} files were updated'.format(num_updated))
            
            if num_updated > 0:
                blog.rebuild_homepage(post_template)


    # MODE: rebuild the entire index
    elif cli_args.rebuild_index == True:
        if cli_args.force == True:
            blog.log('REBUILD w/FORCE-d recompile')
        else:
            blog.log('REBUILD changed source only')
        
        num_updated = 0
        
        # Get all published source files
        all_source_files = []
        for (path, dirs, files) in os.walk(blog.published_dir):
            # No dotfiles
            files = filter(lambda f: f[0:1] != '.', files)            
            
            for f in files:
                all_source_files.append(os.path.join(path, f))
        
        # For each file:
        #   Get cache location
        #   For each, compare to cache
        #   If FORCE or MODIFIED, recompile using the template
        #   Add the file to lists for rebuilding indexes
        for source_file in all_source_files:
            if cli_args.force or blog.different_from_cache(source_file):
                num_updated += blog.update_published_post(source_file, post_template)
        
        # Remove HTML which does not match a source file
        blog.remove_unmatched_html()
        
        # Rebuild indexes
        blog.log('{:d} {:s} updated'.format(num_updated, 
            'file was' if num_updated == 1 else 'files were'))
        
        if num_updated > 0:
            blog.rebuild_homepage(post_template)
    
    # MODE: clean out preview directory
    elif cli_args.clean_previews == True:
        blog.log('CLEAN PREVIEWS')
        blog.remove_old_preview_html()


def parse_command_line_args():
    parser = argparse.ArgumentParser(description="CLI tool for the staticblog engine.")
    parser.add_argument('--force', action='store_true', help='force rebuilding of HTML for unmodified source')
    parser.add_argument('FILES',   nargs='*',           help='(optional) files to be operated on')
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--drafts',         action='store_true', help='scan draft posts for changes')
    group.add_argument('--published',      action='store_true', help='scan published posts for changes')
    group.add_argument('--rebuild-index',  action='store_true', help='rebuild the entire post index')
    group.add_argument('--clean-previews', action='store_true', help='remove old preview HTML files')
    
    return parser.parse_args(sys.argv[1:])


if __name__ == '__main__':
    updater()
