# Class for handling Markdown blog posts.
# The assumed format for a post is the following:
# 
# The Title is on Line One
# ========================
# meta-key: value
# meta-key-only
# 
# Post contents follow for the remainder of the 
#
# Markdown source will be filed away with the format
# YYYYMMDD-NN-SLUG.md
#
# Permalinks are /YYYY/MM/slug.html


from datetime import datetime

import markdown
import os
import re


class BlogPost:
    
    timestamp_format = '%Y-%m-%d %I:%M:%S %p'
    
    # If a file name is supplied, the post is inited with contents.
    def __init__(self, file=None):
        # Contents
        self.filename     = None
        self.source       = None
        self.html         = None
        # Meta
        self.title        = None
        self.status       = None
        self.published    = None
        self.tags         = []
        self.meta         = dict()
        
        if file is not None: self.init_from_file(file)
    
    
    def __str__(self):
        return '{:s} ({:s}, {:s})'.format(self.title,
            os.path.basename(self.filename), self.published_str)
    
    __repr__ = __str__
    
    
    # Fill out the post's properties from a file.
    # This is the usual way of doing this, called from __init__
    def init_from_file(self, filename):
        with open(filename, 'r') as f:
            file_contents = ''.join(f.readlines()).decode('utf8')
        
        self.filename = filename
        self.source   = file_contents

        title, meta, body = self.separate_file_contents(file_contents)
        self.title = title
        
        parser = self.setup_parser()
        self.html = parser.convert(body)
        
        self.parse_metadata(meta)


    # Here we assume the file has a file extension (e.g. ".md")
    # We also can remove prefixes of the format "YYYYMMDD-NN-"
    # which shouldn't be part of the slug.
    @property
    def slug(self):
        filename = os.path.basename(self.filename)
        
        # 1. Remove ".xyz" from the filename
        # 2. Remove bracketing spaces
        # 3. Normalize contained spaces to dashes
        slug = re.sub(r'[ ]+', '-', '.'.join(filename.split('.')[:-1]).strip())
        
        # Remove the date and numbering component
        slug = re.sub(r'^[0-9]{8}-[0-9]{2}-', '', slug)
        return slug
        

    def setup_parser(self):
        md = markdown.Markdown(extensions=['smarty','footnotes'], output_format='html5')
        return md

    
    # The metadata can be either "key: value" or "key" and is
    # passed to the method as a multiline string
    def parse_metadata(self, metadata):
        meta_dict = {}
        pieces = metadata.strip().split('\n')
        
        for p in pieces:
            kvsplit = map(lambda s: s.strip(), p.split(':'))
            
            if len(kvsplit) == 1:
                # key type
                meta_dict[kvsplit[0].lower()] = None
            elif len(kvsplit) == 2:
                # key-value type
                meta_dict[kvsplit[0].lower()] = kvsplit[1]
            else:
                meta_dict[kvsplit[0].lower()] = ':'.join(kvsplit[1:])

        # Special meta types
        if 'published' in meta_dict.keys():
            self.published = self.datetime_from_timestamp(meta_dict['published'])
            del meta_dict['published']

        if 'tags' in meta_dict.keys():
            tags = []
            # Normalize by converting [ ]+ to '-' and converting to lowercase
            for subtag in map(lambda s: s.strip(), meta_dict['tags'].split(',')):
                tags.append(re.sub(r'[\s]+', '-', subtag).lower())
            
            del meta_dict['tags']
            self.tags = sorted(list(set(tags)))
        
        # All other meta gets stored in a catch-all
        if len(meta_dict.keys()) > 0:
           self.meta = meta_dict

    
    # Make a datetime object from a file's timestamp
    def datetime_from_timestamp(self, timestamp):
        return datetime.strptime(timestamp, self.timestamp_format)
    
    
    # Split the Markdown into (1) title, (2) metadata, and (3) body
    @staticmethod
    def separate_file_contents(source):
        lines = source.lstrip().split('\n')
        
        title = lines[0].strip()
        
        # Starting from 2 since skipping title and "======="
        meta_start = 2
        meta_end   = None
        
        for i in xrange(meta_start, len(lines)):
            if len(lines[i].strip()) == 0:
                meta_end = i
                break
        
        meta = '\n'.join(lines[meta_start:meta_end])
        body = '\n'.join(lines[meta_end+1:])
        
        return title, meta, body

    
    # Access the publish date as a formatted string
    @property
    def published_str(self):
        return datetime.strftime(self.published, self.timestamp_format)

    
    def set_published(self, date=None):
        if date is None: date = datetime.now()
        self.published = date
    
    
    # Permalink
    @property
    def permalink(self):
        if self.published is None: return None
        permalink = '%04d/%02d/%s' % (self.published.year, self.published.month, self.slug)
        return permalink

    
    # Prettify the source.
    # This routine strips out key-only meta.
    @property
    def formatted_source(self):
        source  = self.title          + '\n'
        source += '='*len(self.title) + '\n'

        # Meta
        if len(self.tags) > 0:
            source += '{:s}: {:s}\n'.format('Tags', ', '.join(self.tags))
        
        if self.published is not None:
            source += '{:s}: {:s}\n'.format('Published', self.published_str)
        
        # Get rid of key-only metadata and print remaining bits
        if len(self.meta.keys()) > 0:
            for key in sorted(self.meta.keys()):
                if self.meta[key] is None:
                    continue
                else:
                    source += "{:s}: {:s}\n".format(key[0:1].upper()+key[1:], self.meta[key])

        source += '\n'
        _, _, body = self.separate_file_contents(self.source)
        source += body
        
        return source
