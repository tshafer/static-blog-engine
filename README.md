# Tom's Static Blogging Engine

This is a static blogging engine that I spent way too much time writing.  It is
mostly a port of [Marco Arment][marco]'s [Second Crack][secondcrack] static
engine (hosted at [GitHub][sc-github]) from PHP to Python. It uses
[Dropbox][dropbox] and [inotify-tools][inotify] in pretty much the same way as
Second Crack.  Details are then tweaked to my liking.

I chose Python because I'm much more familiar with it these days (second only to
Fortran 😀) than [PHP][php-fractal]. I wrote my own engine because it was fun
and I wanted a break from [physics programming][my-research].

## Requirements

The blogging engine requires some kind of URL rewriting in the server, since the
HTML files are linked at /blog/YYYY/MM/post-slug.html.  It was built with modern
(2.7+) Python and the [Python-Markdown][pymarkdown] module.

[marco]:       http://marco.org
[secondcrack]: http://www.marco.org/secondcrack/
[sc-github]:   https://github.com/marcoarment/secondcrack
[dropbox]:     http://dropbox.com
[inotify]:     https://github.com/rvoicilas/inotify-tools
[pymarkdown]:  https://pythonhosted.org/Markdown/
[php-fractal]: http://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/
[my-research]: http://tshafer.com/research/

## Design

Presently, the engine works with files of arbitrary file extension in the following format:

    Blog Post Title (on one line)
    =============================
    meta-key: meta-value
    meta-key-2: meta-value-2
    key-only
     
    This begins the first paragraph of post content.

The software makes assumptions regarding where different classes of documents
live (e.g. Markdown source vs. HTML), but much of this is customizable via the
Python module `config`.  An example is provided (config.py.example) --- simply
put the correct paths in, rename it config.py, and you're ready to go.

--------------------------------------------------------------------------------

## To Be Done

1.  Make an RSS feed.
5.  Make the Markdown files readable on the web at either slug.md or slug.text.
6.  Set up code syntax highlighting.
7.  Clean up and genericize BlogTemplate class.
8.  Clean up BlogPost class.
9.  Make URL re-writing generic?
10. Make index paginated
11. Make tag pages
12. Make archive pages.
13. Clean up VPS situation.
15. Make sure slugs are URL encoded.
16. Expand configuration options (blog title, directory, etc.)
17. Fix BlogPost to set permalink='#' when not published.