# Module for the blogging engine and its operations.


import cgi
import config
import filecmp
import glob
import os
import pytz
import re
import shutil
import sys

from datetime import datetime
from post     import BlogPost
from template import BlogTemplate


# Useful whole-path directories
cache_dir     = os.path.join(config.root, config.paths['cache'])
drafts_dir    = os.path.join(config.root, config.paths['drafts'])
published_dir = os.path.join(config.root, config.paths['published'])
preview_dir   = os.path.join(config.root, config.paths['preview'])
removed_dir   = os.path.join(config.root, config.paths['removed'])
template_dir  = os.path.join(config.root, config.paths['template'], config.template['version'])


# Return the number of published files (0 or 1)
def update_draft_post(file, tmpl):
    
    post = BlogPost(file)
    
    log('Post(Title="{:s}", Slug="{:s}", File={:s})'.format(post.title, post.slug, file))
    
    test_publish = 'publish' in post.meta.keys() \
                or 'publish-now' in post.meta.keys()
    
    # Keep in draft and build a preview
    if not test_publish:
        log('Generating preview')
        
        # Markdown the file and write to preview
        page_html = tmpl.set_post(post, permalink='#')
        page_html = re.sub('/blog/template', '../template/1', page_html)
        
        preview_path = get_preview_path(post)
        write_data_to_file(page_html, preview_path, allow_clobber=True)
        
        return 0
    
    # Publish now
    else:
        log('PUBLISH NOW')
        
        post.set_published()
        
        # Markdown the file
        page_html = tmpl.set_post(post)

        # Write to HTML and do NOT allow overwriting
        html_path = get_html_path(post)
        status = write_data_to_file(page_html, html_path, False)
        
        if status == False: return 0
        
        # Prettify the source and write the archived source file; NO clobber
        clean_markdown = post.formatted_source
        source_path = get_published_source_path(post)
        write_data_to_file(clean_markdown, source_path, False)

        # Cache
        cache_location = get_cache_file_path(source_path)
        copy_file_to_file(source_path, cache_location, True)
        
        # Delete the draft source
        remove_markdown_file(file)
        
        # Delete a companion preview
        preview_path = get_preview_path(post)
        
        if os.path.exists(preview_path):
            os.remove(preview_path)
            log('Deleted preview file {:s}'.format(preview_path))
        
        return 1

# Return the number of changed HTML
def update_published_post(file, tmpl):
    
    post = BlogPost(file)
    
    log('Post(Title="{:s}", Slug="{:s}", File={:s})'.format(post.title, post.slug, file))
    
    # Markdown the file
    page_html = tmpl.set_post(post)
    
    html_path = get_html_path(post)
    if not os.path.exists(html_path):
        log("Warning: this file's HTML did not already exist!")    
    
    status = write_data_to_file(page_html, html_path, True)
    if status == False: return 0

    # Re-cache
    cache_location = get_cache_file_path(file)
    copy_file_to_file(file, cache_location, True)

    return 1

def rebuild_homepage(tmpl):
    index_path = os.path.join(config.wwwroot, 'index.html')
    
    # Find all files
    all_files = []
    for (path, dirs, files) in os.walk(published_dir):
        for file in files:
            if file[0:1] == '.': continue
            all_files.append(os.path.join(path, file))
    
    log('Found {:d} post{:s}'.format(len(all_files), 's' if len(all_files) != 1 else ''))
    
    # Load the post objects, reverse-chronologically sorted
    all_posts = []
    for file in all_files:
        all_posts.append(BlogPost(file))
    all_posts = sorted(all_posts, key=lambda s: s.published_str, reverse=True)
    
    log('Post list: {:s}'.format(', '.join([str(p) for p in all_posts])))
    
    # Make the index file
    index_html = tmpl.set_post_list(*all_posts)
    write_data_to_file(index_html, index_path, True)
    
    # RSS feed
    generate_rss_feed_from_posts(*all_posts)

# Not actually removing files yet, just notifying
def remove_unmatched_html():
    for (path, dirs, files) in os.walk(config.wwwroot):
        if path == config.wwwroot:
            continue
        
        if re.search(r'/[0-9]{4}/[0-9]{2}', path) is None:
            continue
        
        for file in files:
            full_path   = os.path.join(path, file)
            rel_path    = os.path.relpath(full_path, config.wwwroot)
            source_path = re.sub(r'\.html$', '.md', os.path.join(published_dir, rel_path))
            
            if not os.path.isfile(source_path):
                log('!!! HTML file {:s} is a candidate for deletion'.format(rel_path))

def get_cache_file_path(path):
    rel_path   = os.path.relpath(path, published_dir)
    cache_path = os.path.join(cache_dir, rel_path)
    
    return cache_path

def different_from_cache(source_file):
    rel_path   = os.path.relpath(source_file, published_dir)
    cache_path = os.path.join(cache_dir, rel_path)
    
    if not os.path.exists(cache_path):
        log('!!! Could not find cache file {:s} (source file {:s})'.format(cache_path, source_file))
    
    files_same = filecmp.cmp(source_file, cache_path)
    
    if files_same:
        return False
    else:
        return True

def remove_old_preview_html():
    for file in os.listdir(preview_dir):
        file_path = os.path.join(preview_dir, file)
        
        if not os.path.isfile(file_path):
            continue
        
        # Does a corresponding file exist?
        glob_input = os.path.join(drafts_dir, re.sub(r'\.html$', '.*', file))
        num_drafts = len(glob.glob(glob_input))
        
        if num_drafts < 1:
            os.remove(file_path)
            log('Removed HTML preview {:s}'.format(file))

def remove_markdown_file(file_path):
    now = datetime.now()
    dest_path = os.path.join(removed_dir,
        '{:s}-{:s}'.format(now.strftime('%Y%m%d-%H%M%S'),
            os.path.basename(file_path)))
    
    log('Archiving file {:s} to {:s}'.format(file_path, dest_path))
    shutil.move(file_path, dest_path)

def get_preview_path(post):
    return os.path.join(preview_dir, '{:s}.html'.format(post.slug))

def get_html_path(post):
    rel_path = config.blog['html_path'].format(year=post.published.year,
        month=post.published.month, slug=post.slug)
    
    return os.path.join(config.wwwroot, rel_path)

def get_published_source_path(post):
    rel_path = config.blog['source_path'].format(year=post.published.year,
        month=post.published.month, slug=post.slug)
    
    return os.path.join(published_dir, rel_path)

def write_data_to_file(data, file, allow_clobber=False):
    if os.path.exists(file):
        if allow_clobber:
            log('Overwriting file {:s}'.format(file))
        else:
            log('!!! Not allowed to overwrite the file {:s}'.format(file))
            return False
    
    parent_dir = os.path.dirname(file)

    if not os.path.exists(parent_dir):
        make_directory_tree(parent_dir)

    with open(file, 'w') as f:
        f.write(data.encode('utf8'))

    log('Wrote file {:s}'.format(file))
    return True

def copy_file_to_file(file_source, file_dest, allow_clobber=False):
    if os.path.exists(file_dest):
        if allow_clobber:
            log('Overwriting file {:s}'.format(file_dest))
        else:
            log('!!! Not allowed to overwrite the file {:s}'.format(file_dest))
            return False
    
    parent_dir = os.path.dirname(file_dest)

    if not os.path.exists(parent_dir):
        make_directory_tree(parent_dir)

    shutil.copy2(file_source, file_dest)

    log('Copied file {:s} to {:s}'.format(file_source, file_dest))
    return True

def make_directory_tree(path):
    log('Had to build directory tree for {:s}'.format(path))
    os.makedirs(path)

def log(msg, quit=False):
    now = datetime.now()
    
    log_message = '{:s}  {:s}'.format(now.strftime('[%m/%d/%y %H:%M]'), msg)
    
    sys.stderr.write(log_message.rstrip()+'\n')
    
    if config.logfile is not None:
        with open(config.logfile, 'a') as f:
            f.write(log_message.rstrip()+'\n')
    
    if quit:
        quit()

# Note that now we depend on the cgi (builtin) and pytz (not) modules
def generate_rss_feed_from_posts(*posts):
    rfc_format = '%a, %d %b %Y %H:%M:%S %Z'
    sorted_posts = sorted(posts, key=lambda p:p.published, reverse=True)
    
    # pub date is more recent post and last build date is now
    # --- using pytz to normalize to GMT
    most_recent_post = sorted_posts[0]
    pub_date_eastern = pytz.timezone('America/New_York').localize(most_recent_post.published).strftime(rfc_format)
    build_date_eastern = pytz.timezone('America/New_York').localize(datetime.now()).strftime(rfc_format)
    
    # RSS posts
    rss_body = ''
    for ip, p in enumerate(sorted_posts):
        if ip > 0: rss_body += '\n'
        
        p_date = pytz.timezone('America/New_York').localize(p.published).strftime(rfc_format)
        rss_body +=  '<item>'
        rss_body +=  '<title>%s</title>' % p.title
        rss_body +=  '<link>http://tshafer.com/blog/%s</link>' % p.permalink
        rss_body +=  '<pubDate>%s</pubDate>' % p_date
        rss_body +=  '<guid isPermaLink="false">%s</guid>' % p.permalink        
        rss_body += u'<description>%s</description>' % cgi.escape(p.html)
        rss_body +=  '</item>'
    
    # RSS final
    rss_final = u'''
<?xml version="1.0"?><rss version="2.0"><channel><title>Tom Shafer / Blog</title><link>http://tshafer.com/blog/</link><description>Tom Shafer's personal blog.</description><language>en-US</language><pubDate>{pub_date:s} </pubDate><lastBuildDate>{build_date:s} </lastBuildDate><webMaster>hello@tshafer.com (tshafer.com)</webMaster>
{body:s}
</channel></rss>'''.strip().format(pub_date=pub_date_eastern, build_date=build_date_eastern, body=rss_body)
    
    write_data_to_file(rss_final, os.path.join(config.wwwroot,'rss.xml'), allow_clobber=True)
