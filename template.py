#!/usr/bin/env python
#
# Templating class


from datetime import datetime

import config
import os
import string


class BlogTemplate:
    
    allowed_formats = ('post', 'index')
    
    header = ''
    footer = ''
    post   = ''
    
    def __init__(self, format='post'):
        self.format = None
        format = format.strip().lower()
        
        if format not in self.allowed_formats:
            raise ValueError('Template format %s not allowed. Allowed types: %s' \
                % (format, ','.join(self.allowed_formats)))
        else:
            self.format = format
        
        # Load the template parts
        self.load_template()
    
    
    # Current fill-ins are:
    # Header: page_title
    # Footer: (None)
    # Post:   post_permalink, post_title, post_byline, post_body
    def load_template(self):
        header_path = os.path.join(config.root, config.paths['template'],
            config.template['version'], config.template['header'])
        
        footer_path = os.path.join(config.root, config.paths['template'],
            config.template['version'], config.template['footer'])

        post_path = os.path.join(config.root, config.paths['template'],
            config.template['version'], config.template['post'])
        
        with open(header_path, 'r') as f:
            self.header = ''.join(f.readlines())
        
        with open(footer_path, 'r') as f:
            self.footer = ''.join(f.readlines())

        with open(post_path, 'r') as f:
            self.post = ''.join(f.readlines())
    
    
    def set_post(self, post, **kwargs):
        # Make the template
        s  = self.header + '\n'
        s += self.post   + '\n'
        s += self.footer
        tmpl = Template(s)
        
        # Fill-ins
        page_title = '{:s} &mdash; Tom Shafer / Blog'.format(post.title)
        
        post_title = post.title
        post_body  = post.html
        
        if post.published is None: time = datetime.now()
        else:                      time = post.published
        
        post_byline = '{:s} by <a href="/">Tom Shafer</a>'.format(
            time.strftime('%B %d, %Y at %I:%M %p'))
        
        if post.permalink is None:
            post_permalink = '#'
        else:
            post_permalink = '/blog/'+post.permalink.lstrip('/')

        # Overrides
        if 'permalink' in kwargs.keys():
            post_permalink = kwargs['permalink']

        return tmpl.safe_substitute(page_title=page_title, post_title=post_title,
            post_permalink=post_permalink, post_body=post_body, post_byline=post_byline)
    
    
    def set_post_list(self, *posts):
        output = ''
        
        # Header
        page_title = 'Tom Shafer / Blog'
        tmpl = Template(self.header+'\n')
        output += tmpl.safe_substitute(page_title=page_title)
        
        # Post-by-bost
        for ip, post in enumerate(posts):
            if ip > 0: s = '<hr>' + '\n\n'
            else:      s = ''
            
            post_title     = post.title
            post_body      = post.html
            post_permalink = post.permalink
            
            post_byline = '{:s} by <a href="/">Tom Shafer</a>'.format(
                post.published.strftime('%B %d, %Y at %I:%M %p'))
            
            tmpl = Template(s+self.post+'\n')
            output += tmpl.safe_substitute(post_title=post_title, post_permalink=post_permalink,
                post_body=post_body, post_byline=post_byline)
        
        # Footer
        output += self.footer
        
        return output


# StackOverflow #1336786
# Subclass of string.Template
class Template (string.Template):
    delimiter = '$='
    idpattern = r'[a-z][a-z0-9\-_]*'  # Allow dashes
