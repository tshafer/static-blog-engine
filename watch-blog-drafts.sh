#!/usr/bin/env bash
#
# Use inotify to update the blog when /drafts changes.

# See http://unix.stackexchange.com/a/86292
inotifywait -m -r -e moved_to --format="%f" /home/tshafer/Dropbox/staticblog/drafts |
  while read file; do
    staticblog-updater --drafts "/home/tshafer/Dropbox/staticblog/drafts/$file"
  done
